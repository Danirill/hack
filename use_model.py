import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy
import os
from keras.preprocessing import image
import pandas as pd
import tkinter as tk
from tkinter import filedialog
from excel.Reader import createcsv

import pickle


from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, auc
# createcsv('checker', 0)

def getAnswers(SUBS_ID):

    # file_path = filedialog.askopenfilename()
    file_path = './output/endtestendgeneric.csv'
    data = pd.read_csv(file_path)

    data = data.loc[data['SUBS_id'].isin([str(SUBS_ID)])]

    data = data.drop(columns=['SUBS_id'])

    # print(data)

    filename = './output/hack_modelSki.sav'
    loaded_model = pickle.load(open(filename, 'rb'))

    result = loaded_model.predict(data)

    return result
