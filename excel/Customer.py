class Custumer:
    def __init__(self):
        self.SUBS_ID = []

        self.SMS_IN_CNT = []
        self.SMS_OUT_CNT = []

        self.CALLS_IN_CNT = []
        self.CALLS_OUT_CNT = []

        self.DURATION_IN_MIN = []
        self.DURATION_OUT_MIN = []

        self.DATA_TRAFFIC_MB = []

        self.SIM_LTE = []

        self.TARIFF_ID = []

        self.SUPPORT_3G = []
        self.SUPPORT_4G = []

        self.SUBS_HOME_REGION_ID = []

        self.CHARGE = []
        self.RECHARGE = []
        self.RECHARGE_CNT = []

        self.LIFE_TIME = []
        self.MNP_OUT = []

        self.BROKEN = False
        self.DATA_CNT = 0
        self.MONTH = []

    def Update(self, data):
        self.SMS_IN_CNT.append(data[2])
        self.SMS_OUT_CNT.append(data[3])

        self.CALLS_IN_CNT.append(data[4])
        self.CALLS_OUT_CNT.append(data[5])

        self.DURATION_IN_MIN.append(data[6])
        self.DURATION_OUT_MIN.append(data[7])

        self.DATA_TRAFFIC_MB.append(data[8])

        self.SIM_LTE.append(data[9])

        self.TARIFF_ID.append(data[10])

        self.SUPPORT_3G.append(data[11])
        self.SUPPORT_4G.append(data[12])

        self.SUBS_HOME_REGION_ID.append(data[13])

        self.CHARGE.append(data[14])
        self.RECHARGE.append(data[15])
        self.RECHARGE_CNT.append(data[16])

        self.LIFE_TIME.append(data[17])
        try:
            self.MNP_OUT.append(data[18])
        except:
            self.MNP_OUT.append("0")

        self.DATA_CNT += 1
        if data[1] in self.MONTH:
            self.BROKEN = True
        else:
            self.MONTH.append(data[1])
